## Rapid prototyping af digital sensorsystemer
This archive tries to sum up all curriculum of DTU course [22437 Rapid Prototyping of Digital Sensor Systems using Arduino Technology](https://kurser.dtu.dk/course/22437) 2022.

To fully understand code comments, roadmap ect. you should be a part of the DTU course 22437 and therefor have access to the study materials. 

## Installation
- [Arduino](https://www.arduino.cc/en/software) 1.8.19 or higher


## Support
If there are missing anything, then feel free to submit an [issue](https://gitlab.com/kiwimarc/rapid-prototyping-af-digital-sensorsystemer/-/issues) or send me an [email](mailto:contact-project+kiwimarc-rapid-prototyping-af-digital-sensorsystemer-33360453-issue-@incoming.gitlab.com).

## Roadmap
* [x] Week 1
***  
* [x] Week 2
***
* [x] Week 3
***
* [x] Week 4 
***
* [x] Week 5
***
* [ ] Week 6
***
* [x] Week 7
***
* [ ] Week 8
***
* [x] Week 9
***
* [x] Week 10
***
* [x] Week 11
***
* [x] Week 12
***
* [x] Week 13


## Contributors


## Authors and acknowledgment
For every formula used in this project is from the DTU course 22437 and is therefor credited to [Kaj-Åge Henneberg](https://www.dtu.dk/service/telefonbog/person?id=5548&cpid=&tab=1)

## License
[MIT License](https://gitlab.com/kiwimarc/rapid-prototyping-af-digital-sensorsystemer/-/blob/main/LICENSE)

Feel free to use this project in anyway you want.

## Project status
This project will be worked on until the 24th of may 2022. After this date only contributions and suggestions will be added.
