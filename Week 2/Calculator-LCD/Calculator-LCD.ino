// include the library code:
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
const int rs = 7, en = 8, d4 = 9, d5 = 10, d6 = 11, d7 = 12;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

// initialize symbols
byte divide = B11111101;
byte multiply[8] = {
  B00000,
  B00000,
  B10001,
  B01010,
  B00100,
  B01010,
  B10001,
  B00000
};
byte add[8] = {
  B00000,
  B00000,
  B00100,
  B00100,
  B11111,
  B00100,
  B00100,
  B00000
};
byte sub[8] = {
  B00000,
  B00000,
  B00000,
  B00000,
  B11111,
  B00000,
  B00000,
  B00000
};
byte equal[] = {
  B00000,
  B00000,
  B00000,
  B11111,
  B00000,
  B11111,
  B00000,
  B00000
};


void setup() {
  // initialize LCD and set up the number of columns and rows: 
  lcd.begin(16, 2);

  // create a new character
  lcd.createChar(0, divide);
  // create a new character
  lcd.createChar(1, multiply);
  // create a new character
  lcd.createChar(2, add);
  // create a new character
  lcd.createChar(3, sub);
  // create a new character
  lcd.createChar(4, equal);

  // Clears the LCD screen
  lcd.clear();

  // Print a message to the lcd.
  lcd.print("The calculator");
  Serial.begin(250000);
  while (! Serial);  // Wait until Serial is ready 
}

int val1;
char op;
int val2;
int result;

void loop() {
  while(Serial.available() > 0) {
      // Print a message to the lcd.
      val1 = Serial.parseInt();
      op = Serial.read();
      val2 = Serial.parseInt();
      if (op == '+') {
        result = val1 + val2;
        lcd.setCursor(1, 1);
        lcd.print(val1);
        lcd.setCursor(2, 1);
        lcd.write(byte(2)); // operator
        lcd.setCursor(3, 1);
        lcd.print(val2);
        lcd.setCursor(4, 1);
        lcd.write(byte(4));
        lcd.setCursor(5, 1);
        lcd.print(result);
      }
    
      if (op == '-') {
        result = val1 - val2;
        lcd.setCursor(1, 1);
        lcd.print(val1);
        lcd.setCursor(2, 1);
        lcd.write(byte(3)); // operator
        lcd.setCursor(3, 1);
        lcd.print(val2);
        lcd.setCursor(4, 1);
        lcd.write(byte(4));
        lcd.setCursor(5, 1);
        lcd.print(result);
      }
    
      if (op == '*') {
        result = val1 * val2;
        lcd.setCursor(1, 1);
        lcd.print(val1);
        lcd.setCursor(2, 1);
        lcd.write(byte(1)); // operator
        lcd.setCursor(3, 1);
        lcd.print(val2);
        lcd.setCursor(4, 1);
        lcd.write(byte(4));
        lcd.setCursor(5, 1);
        lcd.print(result);
      }
    
      if (op == '/') {
        result = val1 / val2;
        lcd.setCursor(1, 1);
        lcd.print(val1);
        lcd.setCursor(2, 1);
        lcd.write(byte(0)); // operator
        lcd.setCursor(3, 1);
        lcd.print(val2);
        lcd.setCursor(4, 1);
        lcd.write(byte(4));
        lcd.setCursor(5, 1);
        lcd.print(result);
      }
  }
}
