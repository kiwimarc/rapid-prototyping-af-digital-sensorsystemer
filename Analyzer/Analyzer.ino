#include <LiquidCrystal.h> // <LCD
#include <SPI.h> // Enable some <<<<sd cards. We don't use this
#include <SD.h> // SD
//===================
//Declarations for SD
//===================
File dataFile;
String fileName = "Data.bin";                 // Datatype der holder tekst
const int CS_PIN = 10;                        //Chip Select Pin Green wire. const gør at denne værdi ikke kan ændres - den er konstant

//These are set by default via the SD card library
//MOSI = Pin 11 
//MISO = Pin 12 
//SCLK = PIN 13 

//====================
//Declarations for LCD
//====================
const int rs = 4, en = 5, d4 = 6, d5 = 7, d6 = 8, d7 = 9;  // værdier sættes til konstant igen 
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);          //siger hvilke pins, som vi arbejder på

//===============================
//Declaration of global variables
//===============================

bool      usePlotter              = true;   //If true; plotter; if false monitor
uint16_t  lcdDelay                = 1000;   //Time delay when displaying on LCD
uint16_t  FS = 500;                         //Sampling frequency
float     dt = 1000.0/FS;                   //Milliseconds between samples
String    text1, text2;                     //For printing to LCD
uint16_t  YData                   = 0;      //For plotting
uint16_t  minY                    = 0;      //Minimum value on y-axis
uint16_t  maxY                    = 150;    //Maximum value on y-axis
byte      lsb, msb;                         //LSByte and MSbyte from data file

// moving averaging
const int maBufferSize            = 10;     //Width of moving average window
uint16_t  maBuffer[maBufferSize];           //Moving average buffer
uint16_t  sampleCounter           = 0;      //Counts number of samples
uint16_t  maBufferIndex           = 0;      //Pointer to sample in buffer
float     maYData                 = 0;      //Moving average value

// peak detection
uint16_t  YThreshold              = 90;    //Threshol for R-peak
bool      newRpeak                = false;  //For making peak above threshold
bool      RpeakFlag               = false;  //For making peak above threshold
uint16_t  RRsamples               = 0;      //Number of samples between R peaks
uint16_t  newRPeakIndex           = 0;      //Index of latest peak
uint16_t  oldRPeakIndex           = 0;      //Index pf previous peak
float     RRTime                  = 0;      //Time in ms between R peaks
float     HR                      = 0;      //Heart rate in beats per minute

//===================
//function propotypes
//===================
void setupLCD(void);            // sætter LCD op. Der er hverken input eller output
void writeLCD(String text1, String text2, uint16_t lcdDelay);
void writeLCD_TOP(String text1, uint16_t lcdDelay);
void writeLCD_BOT(String text1, uint16_t lcdDelay);
void setupSD(void);            // sætter SD op. Der er hverken input eller output
void processDataSerial(void);  // Processere data
void myPlotter(uint16_t YDate, uint16_t maYData, uint16_t minY, uint16_t maxY);
void myPrinter(uint16_t sampleCounter, uint16_t maBufferIndex, uint16_t Y, float maY);


void setup (){
  Serial.begin(115200);
  setupLCD();           // Initialize LCD
  setupSDForReading();  // Initialize SD card
  writeLCD_TOP("ECG analyzer",lcdDelay);
  writeLCD_BOT("dt =" + String(dt),lcdDelay);
  writeLCD_BOT("MA window = " + String(maBufferSize), lcdDelay);
  processDataSerial();
  writeLCD_TOP("Job done", 10);
  writeLCD_BOT("Hasta la vista",10);
}

void loop(){}
void setupLCD (){
  lcd.begin(16,2);
  lcd.clear();
  writeLCD_TOP("LCD",10);
  writeLCD_BOT("ready",10);
}
void writeLCD(String txt1, String txt2, uint16_t Delay) {
  lcd.clear();
  lcd.setCursor(0,0);         // Vores rækker på display gå fra 0 - 15 og (0,0) sætter så cursoren i toppen på plads 0
  lcd.print(txt1);
  lcd.setCursor(0,1);         // Vores rækker på display gå fra 0 - 15 og (0,1) sætter så cursoren i toppen på plads 1
  lcd.print(txt1);
  delay(Delay);
}
void writeLCD_TOP(String txt, uint16_t Delay){
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(txt);
  delay(Delay);
}

void writeLCD_BOT(String txt, uint16_t Delay){
  lcd.setCursor(0,1);
  lcd.print(txt);
  delay(Delay);
}
void setupSDForReading() {
  SD.begin();               //aktivere SD bib
  if (!SD.begin(CS_PIN)){   // ! betyder ikke. Den siger, hvis biblioteket ikke starter, så genstart
    writeLCD_TOP("Card Failure",lcdDelay);
    while(1);
  }
  writeLCD_TOP("Card Ready",lcdDelay);
  dataFile = SD.open(fileName, FILE_READ);  //Åbner filen, hvor vi kun kan læse den
  if (dataFile) {
    writeLCD_TOP("File exist",lcdDelay);    // Fil eksistere
  }
  else {
    writeLCD_TOP("File does not exist",lcdDelay);
  }
}
void processDataSerial() {
  while(dataFile.available()){
    lsb= dataFile.read();   // Read LSByte
    msb = dataFile.read();  // ReadMSByte
    YData = (msb<<8) + lsb; //Combine to an ind. YData skal være en float, altså en værdi, hvorfor msb og lsb samles til en byte (0 - 7)
    movingAverage();        //Calculate moving average
    if (usePlotter){        //Print to Serial plotter
      myPlotter(minY, maxY,YThreshold, YData, maYData);
    }
    else{                   // Print to Serial monitor
      myPrinter(sampleCounter, maBufferIndex, YData, maYData);
    }
    if (maYData > YThreshold + 10 && !RpeakFlag){ 1     //Threshold sætter en grænse, så P-takken ikke tæller med
      newRPeakIndex = sampleCounter;
      RpeakFlag = true;     // Raise the thresshold crossing flag
      RRsamples = newRPeakIndex - oldRPeakIndex; //Number of smaples between R-peaks
      RRTime = dt * RRsamples;    // Time duration between R-peaks
      HR = 6000.0/RRTime;         // Heart rate
      writeLCD("R_R : " + String(RRTime),"HR : " + String(HR),10);
    }
    if (maYData < YThreshold - 10 && RpeakFlag){        //KLar til at måle ny puls
      RpeakFlag = false;          // Find and mark downward thresshold crossing 
      oldRPeakIndex = newRPeakIndex; // Save the found R-peak index 
    }
  }
}

void movingAverage(){


  if (sampleCounter < maBufferSize){
    maYData = (float) (YData + sampleCounter*maYData)/(sampleCounter + 1);
  }
  else{
    maYData = maYData + ((float)YData - (float)maBuffer[maBufferIndex])/maBufferSize;
  }
  maBuffer[maBufferIndex] = YData;        //Store new sample in averaging array
  sampleCounter += 1;                     //Increment counter 
  maBufferIndex = sampleCounter % maBufferSize;    // Update index into smaples array
}


void myPlotter(uint16_t minY, uint16_t maxY, uint16_t YThreshold, uint16_t Y, float maY){
  Serial.print(minY);           Serial.print(",");
  Serial.print(Y);              Serial.print(",");
  Serial.print(maY);            Serial.print(",");
  Serial.print(YThreshold);     Serial.print(",");
  Serial.println(maxY);
}

void myPrinter(uint16_t sampleCounter, uint16_t maBufferIndex, uint16_t Y, float maY){
  Serial.print(sampleCounter);  Serial.print("\t");
  Serial.print(maBufferIndex);  Serial.print("\t");
  Serial.print(Y);              Serial.print("\t");
  Serial.println(maY);
}
