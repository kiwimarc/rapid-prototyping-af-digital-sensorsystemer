#include<LiquidCrystal.h> // Include LCD library 
#include<SPI.h> // Include SPI library
#include<SD.h> // Include SD library

const int rs = 4, en = 5, d4 = 6, d5 = 7, d6 = 8, d7 = 9, csPin = 10, FS=500;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
String fileName = "Data.bin";
int maxSamples = 4992;                                    //hævet til 4992
File dataFile;
volatile int resultNumber = 0; // Gemmer i RAM
volatile int blockSize = 32;
volatile bool saveBlock0ToSD = false;
volatile int adcBlock0[32];
volatile int adcBlock1[32];
volatile bool notSavedYet = false;
bool isDebugging = false;
const byte adcPin = 0;
uint16_t lsb, msb, YData, minY = 80, maxY = 160;          //minY og maxY initialiseret med værdier
byte preScalerT1Bits = 0; // Timer 1 prescalar bits CS[2:0]
int preScalerT1 = 1;
int preScalerCS1 = 0;
volatile int adcData = 0;
uint16_t adcPrescaler = 128;

// funtions prototype
void setupLCD(void);
void writeLCD_TOP(String text1);
void writeLCD_BOT(String text1);
void setupSD(void);
void setupTimer(void);
void StartTimer(void);
void setupADC(void);
void plotDataSerial(void);
void myPlotter(int YData, int Ymin, int Ymax);

void setup(){
  Serial.begin(115200);     // Baud rate er sende hastigheden af data
  setupLCD();               // Initialize LCD
  setupSD();                // Initilize SD card
  writeLCD_TOP("FS = " + String(FS));
  writeLCD_BOT("Samples : " + String(maxSamples));
  delay(2000);
  setupTimer();             // initialize timer
  setupADC();               // initialize ADC
  delay(2000);              // wait for interupts to be enabled

  dataFile = SD.open(fileName, FILE_WRITE);     //Skriver til SD-kortet (ikke nødvendigt)
  if (dataFile){
    writeLCD_TOP("Start Timer");
    startTimer();           // Start timer and conversion
  }
  else{
    writeLCD_TOP("File open failed");
    while(1);               // If file open failes get trapped here
  }
}

ISR (ADC_vect){
  int j;
  if (resultNumber >= maxSamples){
    ADCSRA = 0;             // Turn off ADC
  }
  else {
    j = resultNumber % blockSize;
    if (!saveBlock0ToSD){     // ! er lig ikke
      adcBlock0[j] = ADC;       
    }
    else {
      adcBlock1[j] = ADC;
    }
    resultNumber++;
  }
  if (j == blockSize -1){
    saveBlock0ToSD = !saveBlock0ToSD; // laver den fra false => true
    notSavedYet = true;
  }
}

ISR(TIMER1_COMPB_vect){}        
void loop () {
  if (notSavedYet){
    if(saveBlock0ToSD){
      dataFile.write((const uint8_t *)&adcBlock0, sizeof(adcBlock0)); //binary file
    }
    else{
      dataFile.write((const uint8_t *) adcBlock1, sizeof(adcBlock1)); //Binary file
    }
    notSavedYet = false;
  }
  if (ADCSRA == 0){               // If ADC is done
    dataFile.close();             // Close data file after sampling is done
    writeLCD_BOT("Sampling done");
    delay(2000);
    writeLCD_BOT(fileName + "closed");
    if (isDebugging){
      plotDataSerial();          //Plot data to serial port
    }
    writeLCD_TOP("Job done");
    writeLCD_BOT("Asta la vista");
    while(1);
  }
}

void setupLCD (){
  lcd.begin(16,2);
  lcd.clear();
  writeLCD_TOP("LCD");
  writeLCD_BOT("ready");
}
void setupSD (){
  pinMode(csPin, OUTPUT);
  SD.begin();   
  if (!SD.begin(csPin)) {
    writeLCD_TOP("SD card error");
    while(1);
  }
  writeLCD_TOP("SD card ready");
  
  dataFile = SD.open(fileName,FILE_WRITE); // Open file
  if (dataFile) {
    writeLCD_TOP("File exists");
    writeLCD_BOT("Removing file");
    SD.remove(fileName);
    delay(20);
  }
  writeLCD_TOP("Creating file");
  dataFile = SD.open(fileName,FILE_WRITE); // Open file
  if (dataFile){
  dataFile.close();
  writeLCD_TOP("new file" + fileName);
  }
}

void writeLCD_TOP(String txt){
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(txt);
  delay(500);
}

void writeLCD_BOT(String txt){
  lcd.setCursor(0,1);
  lcd.print(txt);
  delay(500);
}

void setupTimer(){
  writeLCD_TOP("Setup Timer");

  cli();                      // disable interrupt
  TCCR1A  = 0;                 // set entire TCCR1A register to 0
  TCCR1B  = 0;                 // same for TCCR1B
  TCCR1B |= (1 << WGM12);     // turn on CTC mode for Timer1
  
  if (FS >= 500) {            // Desto højere samplingfrekvens desto lavere prescaler
      // Set CS12 CS11 CS10 bits for prescaler value 1
      preScalerT1Bits |= (0 << CS12) |(0 << CS11) | (1 << CS10);
      preScalerT1 = 1;
      preScalerCS1 = 1;       // CS1[2:0] 
  }
  if (FS >= 50 && FS < 500) { 
      // Set CS12 CS11 CS10 bits for prescaler value 8
      preScalerT1Bits |= (0 << CS12) |(1 << CS11) | (0 << CS10);
      preScalerT1 = 8;
      preScalerCS1 = 2;       // CS1[2:0]  
  }
  if (FS >= 5 && FS < 50) { 
      // Set CS12 CS11 CS10 bits for prescaler value 64
      preScalerT1Bits |= (0 << CS12) |(1 << CS11) | (1 << CS10);
      preScalerT1 = 64;
      preScalerCS1 = 3;       // CS1[2:0]  
  }
  if (FS >= 1 && FS < 5) { 
      // Set CS12 CS11 CS10 bits for prescaler value 256
      preScalerT1Bits |= (1 << CS12) |(0 << CS11) | (0 << CS10);
      preScalerT1 = 256;
      preScalerCS1 = 4;       // CS1[2:0]  
  }
  // set compare match register to match sampling frequency
  OCR1A = (16000000) / (FS * preScalerT1) - 1; // Udregner TOP værdien sammenhæng med sampling frekvens
  OCR1B = OCR1A;
  TCNT1  = 0;                 //initialize counter value to 0
  TIMSK1 = 0;
  TIMSK1 |= (1 << OCIE1B);    // enable timer compare interrupt to execute ADC ISR. (Scriptet kørers ikke igennem, når denne fjernes)
  sei(); // aktiver interrups
  delay(2000);
  writeLCD_BOT("Setup Done");   //khen: TOP ændret til BOT
 }

void startTimer(){
   TCCR1B |= preScalerT1Bits;
}

void setupADC(){
  writeLCD_TOP("Setup ADC");
  cli();                        //Clear interrupt
  analogReference(DEFAULT);     // Set ADC voltage reference to 5 V
  adcData = analogRead(adcPin); // Discard first conversion

  DIDR0 |= (1 << adcPin);   //Digital input disabled on all ADC ports
  ADCSRA |= ((1 << ADEN) | (1 << ADIE) | (1 << ADIF));  // turn ADC on, enable interrupt, lower flag 
  ADCSRA &= ~((1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0));    // clear ADC prescalar bits

  if (FS <= 8000) {
    ADCSRA |= ((1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0));
    adcPrescaler = 128;
  }
  if (FS > 8000 && FS <= 16000) {
    ADCSRA |= ((1 << ADPS2) | (1 << ADPS1) | (0 << ADPS0));
    adcPrescaler = 64;
  }
  if (FS > 16000 && FS <= 32000) {
    ADCSRA |= ((1 << ADPS2) | (0 << ADPS1) | (1 << ADPS0));
     adcPrescaler = 32;
  }
  if (FS > 32000 && FS < 64000) {
    ADCSRA |= ((1 << ADPS2) | (0 << ADPS1) | (0 << ADPS0));
    adcPrescaler = 16;
  }
   if (FS > 64000 && FS < 128000) {
    ADCSRA |= ((0 << ADPS2) | (1 << ADPS1) | (1 << ADPS0));
    adcPrescaler = 8;
  }
   if (FS > 128000 && FS < 256000) {
    ADCSRA |= ((0 << ADPS2) | (1 << ADPS1) | (0 << ADPS0));
    adcPrescaler = 4;
  }
   if (FS > 128000 && FS < 256000) {
    ADCSRA |= ((0 << ADPS2) | (0 << ADPS1) | (1 << ADPS0));
    adcPrescaler = 2;
  }  
  ADMUX &= ~((1<<REFS1) | (1<<REFS0) | (1<<ADLAR) | (1<<MUX3) | (1<<MUX2) | (1<<MUX1) | (1<<MUX0));
  ADMUX |= ((1<<REFS0) | (adcPin & 7));     // Sets voltage reference and ADC pin
  // select the A/D trigger source: timer1 compare B match (ADTS[2:0]=101)
  // A/D conversion is started when match occurs
  ADCSRB |= (0 << ACME) | (1 << ADTS2) | (0 << ADTS1) | (1 << ADTS0);
  ADCSRA |=  (1<<ADATE);                    // turn on automatic triggering
  sei();            // enable interrupt
  writeLCD_BOT("Setup done");
} 

void plotDataSerial(){
  Serial.println("minY: Y: maxY:");
  SD.begin(); 
  if (!SD.begin(csPin)){
    writeLCD_TOP("Card Failure");
    while(1);
  }
  writeLCD_TOP("Card Ready");
  dataFile = SD.open(fileName, FILE_READ);
  long int fileSize = dataFile.size();
  if (dataFile){
    writeLCD_TOP ("File size : " + String(fileSize));
    writeLCD_BOT("Plot data");
    while(dataFile.available()){
      lsb = dataFile.read();
      msb = dataFile.read();
      YData = (msb << 8) + lsb;
      myPlotter(YData, minY, maxY);
      delay(1);
    }
    dataFile.close();
    writeLCD_BOT("Close " + fileName);
  }
  else {
    writeLCD_TOP("Couldn't open" + fileName);
  }
}

void myPlotter (uint16_t Y, uint16_t minY, uint16_t maxY){
  Serial.print(minY);     Serial.print(",");
  Serial.print(Y);        Serial.print(",");
  Serial.println(maxY);             // print rettet til println
}
