#include <LiquidCrystal.h> // Include LCD library

const int rs = 7, en = 8, d4 = 9, d5 = 10, d6 = 11, d7 =12;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7); 
byte degree = B11011111, omega = B11110100;

const float a1 = 3.354016E-03, b1 = 2.569850E-04, c1 = 2.620131E-06, d1 = 6.383091E-08; // NTC coefficient
const float Rfix = 10.0E3; // Fixed  resistor
const float VCC = 5.0; // Voltage supply
float R0 = 10.0E3; // NTC resistance at 25 C
float RT = R0; // NTC running value
float T = 0;
float VT = 0; // Thermistor voltage out

const int BLED = 2, GLED = 3, RLED = 4, NTC = 0; // Cathode pins
const int lowerBound = 1000, upperBound = 5000; // Thresholds
int Avalue = 0; // Holds analog reads
float minValue, maxValue, meanValue;

void setup() {
  Serial.begin(9600);
  while(!Serial); // Wait til serial are ready

  // initialize LCD and set up the number of columns and rows: 
  lcd.begin(16, 2);

  // Clears the LCD screen
  lcd.clear();

  // Print a message to the lcd.
  lcd.print("Light Meter");

  Avalue = analogRead(0);
  pinMode(RLED, OUTPUT);
  pinMode(GLED, OUTPUT);
  pinMode(BLED, OUTPUT);

  digitalWrite(RLED, HIGH);
  digitalWrite(GLED, LOW);
  digitalWrite(BLED, LOW);
  delay(150);
  digitalWrite(RLED, LOW);
  digitalWrite(GLED, HIGH);
  digitalWrite(BLED, LOW);
  delay(150);
  digitalWrite(RLED, LOW);
  digitalWrite(GLED, LOW);
  digitalWrite(BLED, HIGH);
  delay(150);
  digitalWrite(RLED, LOW);
  digitalWrite(GLED, LOW);
  digitalWrite(BLED, LOW);
  delay(250);
}

void loop() {
  Avalue = analogRead(0);
  VT = val2VT(Avalue);
  RT = V2R(VT);
  T = R2T(RT);
  
  minValue  = T;
  meanValue = T;
  maxValue  = T;
  float sumvalue = 0;
  
  for(int i = 0; i < 10; i += 1) {
    Avalue = analogRead(0);
    VT = val2VT(Avalue);
    RT = V2R(VT);
    T = R2T(RT);

    sumvalue += T;
    
    if(T < minValue) {
      minValue = T;
    }
    if(T > maxValue) {
      maxValue = T;
    }
  }
  meanValue = sumvalue/10;
  printSerial(minValue, meanValue, maxValue);
  lcd.clear();
  
  lcd.setCursor(0,0);
  lcd.print("Min:");
  lcd.setCursor(5,0);
  lcd.print("Mean:");
  lcd.setCursor(11,0);
  lcd.print("Max:");
  
  lcd.setCursor(0,1);
  lcd.print(minValue,1);
   lcd.setCursor(5,1);
  lcd.print(meanValue,1);
   lcd.setCursor(11,1);
  lcd.print(maxValue,1);

  if(meanValue < lowerBound) { // Blue
    digitalWrite(RLED, HIGH);
    digitalWrite(GLED, HIGH);
    digitalWrite(BLED, LOW);
  }
  else if(meanValue > upperBound) { // Red
    digitalWrite(RLED, LOW);
    digitalWrite(GLED, HIGH);
    digitalWrite(BLED, HIGH);
  }
  else { // Green
    digitalWrite(RLED, HIGH);
    digitalWrite(GLED, LOW);
    digitalWrite(BLED, HIGH);
  }

  delay(250);
}

void printSerial(float minValue, float meanValue, float maxValue) {
  Serial.print("min:"); Serial.print(minValue); Serial.print(", ");
  Serial.print("mean:"); Serial.print(meanValue); Serial.print(", ");
  Serial.print("max:"); Serial.print(maxValue); Serial.print(", ");
  Serial.println();
}
float val2VT(int value) {
  return (VCC * value) / 1024;
}
float V2R(float v) {
  if (v > 0) {
    return Rfix * (VCC - v) / v;
  }
  else { return -1.0; }
}
float R2T(float RT) {
  float T;
  float x = log(RT / R0);
  float y = a1 + (b1 + (c1 + d1*x)*x)*x;
  T = (1/y) -273.15;
  return T;
}
