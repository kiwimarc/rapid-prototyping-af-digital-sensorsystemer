#include <LiquidCrystal.h> // Include LCD library

const int rs = 7, en = 8, d4 = 9, d5 = 10, d6 = 11, d7 =12;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7); 
byte degree = B11011111, omega = B11110100;

const float a1 = 3.354016E-03, b1 = 2.569850E-04, c1 = 2.620131E-06, d1 = 6.383091E-08; // NTC coefficient
const float Rfix = 10.0E3; // Fixed  resistor
const float VCC = 5.0; // Voltage supply
float R0 = 10.0E3; // NTC resistance at 25 C
float RT = R0; // NTC running value
float T = 0;
float VT = 0; // Thermistor voltage out

const int BLED = 2, GLED = 3, RLED = 4, NTC = 0; // Cathode pins
const int lowerBound = 400, upperBound = 520; // Thresholds
int Avalue = 0; // Holds analog reads

void setup() {
  Serial.begin(9600);
  while(!Serial); // Wait til serial are ready

  // initialize LCD and set up the number of columns and rows: 
  lcd.begin(16, 2);

  // Clears the LCD screen
  lcd.clear();

  // Print a message to the lcd.
  lcd.print("Digital thermistor");

  Avalue = analogRead(0);
  pinMode(RLED, OUTPUT);
  pinMode(GLED, OUTPUT);
  pinMode(BLED, OUTPUT);

  digitalWrite(RLED, HIGH);
  digitalWrite(GLED, LOW);
  digitalWrite(BLED, LOW);
  delay(150);
  digitalWrite(RLED, LOW);
  digitalWrite(GLED, HIGH);
  digitalWrite(BLED, LOW);
  delay(150);
  digitalWrite(RLED, LOW);
  digitalWrite(GLED, LOW);
  digitalWrite(BLED, HIGH);
  delay(150);
  digitalWrite(RLED, LOW);
  digitalWrite(GLED, LOW);
  digitalWrite(BLED, LOW);
  delay(250);
}

void loop() {
  Avalue = analogRead(0);
  VT = val2VT(Avalue);
  RT = V2R(VT);
  T = R2T(RT);
  printSerial(Avalue, VT);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(T,3);
  lcd.write(degree);
  
  lcd.setCursor(0,1);
  lcd.print(RT,3);
  lcd.write(omega);

  if(Avalue < lowerBound) { // Blue
    digitalWrite(RLED, HIGH);
    digitalWrite(GLED, HIGH);
    digitalWrite(BLED, LOW);
  }
  else if(Avalue > upperBound) { // Red
    digitalWrite(RLED, LOW);
    digitalWrite(GLED, HIGH);
    digitalWrite(BLED, HIGH);
  }
  else { // Green
    digitalWrite(RLED, HIGH);
    digitalWrite(GLED, LOW);
    digitalWrite(BLED, HIGH);
  }

  delay(2000);
}

void printSerial(int value, float v) {
  Serial.print("Int value: ");
  Serial.print(value);
  
  Serial.print(" V = ");
  Serial.println(v, 3);
}
float val2VT(int value) {
  return (VCC * value) / 1024;
}
float V2R(float v) {
  if (v > 0) {
    return Rfix * (VCC - v) / v;
  }
  else { return -1.0; }
}
float R2T(float RT) {
  float T;
  float x = log(RT / R0);
  float y = a1 + (b1 + (c1 + d1*x)*x)*x;
  T = (1/y) -273.15;
  return T;
}
