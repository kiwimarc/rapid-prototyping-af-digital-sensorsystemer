// One channel DAQ using Timer 1
// Sampling frequency and duration of sampling sequence set here
// Uses serial port communication with Serial Plotter
// Uses LiquidCrystal_I2C to display status to user (uses A4+A5).
// If you use a standard LCD, you need to modify the LCD commands.
// Uses functions to setup Timer and ADC.
// Sets LED on pin 11 HIGH when not converting.
// Sets LED on pin 12 HIGH when converting.
//
// Approach
//    ADC uses external trigger: Timer1 CTC when TCNT1 = OCR1B.
//
// Test
//    Use a function generator. Set a "Ramp Up" signal of 10 Hz 
//    and 1 V (peak) and connect to analogIn on Arduino.
//
// Kaj-Åge Henneberg
// June 18, 2021.

// Declarations
#include <Wire.h> 
#include <LiquidCrystal.h>

// function prototypes                    -------------------

void setupTimer(uint16_t FS);
void startTimer(void);
void setupADC(uint16_t adcPin, uint16_t FS);
void setupLCD_I2C(void);
void writeLCD_I2C(String text1, String text2);

const int rs = 5, en = 6, d4 = 7, d5 = 8, d6 = 9, d7 =10;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
int value = 0;
int FS = 1000;            // Sampling frequency
int numSamples = 1000;    // Max samples to read
String text1, text2;      // For printing to LCD

// Declare Timer1 variables               ------------------- 

byte preScalerT1Bits = 0; // Timer 1 prescalar bits CS[2:0]
int preScalerT1 = 1;
int preScalerCS1 = 0;
int TimerClockPin = 12;   // Timer 1 sends out clock signal on pin 12

// Declare ADC variables                  -------------------
uint16_t analogIn = 0;
uint16_t adcPrescaler = 128;
volatile int adcData = 0;
volatile uint16_t resultNumber = 0;

void setup() {

  // Pin configuration                    -------------------
  
  pinMode(13,OUTPUT);
  pinMode(12,OUTPUT);       // Green LED on while sampling
  pinMode(11,OUTPUT);       // Red LED on while not sampling
  digitalWrite(13, LOW);
  digitalWrite(12, LOW);    // Turn off LED for ADC conversion
  digitalWrite(11, HIGH);   // Turn on LED for IDLE

  //set up LCD                           -------------------
  
  setupLCD_I2C();           // user function: init 8574
  
  // Start serial port and print info to LCD ---------------
  
  Serial.begin(115200);
  writeLCD_I2C_TOP("FS = " + String(FS));
  writeLCD_I2C_BOT("Samples : " + String(numSamples));

  // Set up timer and ADC              -------------------
 
  cli();                    // stop interrupts
  setupTimer(FS);           // user function
  setupADC(analogIn,FS);    // user function 
  sei();                    // allow interrupts: SREG |= (1<<7)
  delay(500);               // wait for interupts to be enabled

  // Timer and ADC setup completed     -------------------

  // Start sampling process            -------------------
   
  startTimer();             // Start timer and conversion                    
  digitalWrite(11, LOW);
  digitalWrite(12, HIGH);   // Turns LED 12 on for ADC ACTIVE                
  writeLCD_I2C_TOP("Timer started"); 

  // Wait for sampling process to stop ------------------- 

  while (ADCSRA != 0){}     // Wait in this while loop until ADC is done
  digitalWrite(12, LOW);    // Turn off ADC active LED
  digitalWrite(11, HIGH);   // Turn on IDLE LED 
  writeLCD_I2C_BOT("Sampling done");
}
ISR (ADC_vect)
{
  if (resultNumber++ >= numSamples){
    ADCSRA = 0;             // Turn off ADC
    digitalWrite(12, LOW); 
  }
  else {
    adcData = ADCL;         // Read ADC low byte output register
    adcData |= (ADCH<<8);   // Read high byte and combine
    Serial.print(adcData);  // Send data point to serial plotter
    Serial.write(13);       // Send Cariage Return
    Serial.write(10);       // Send Linefeed
  }
}
ISR(TIMER1_COMPB_vect){}
// EMPTY_INTERRUPT (TIMER1_COMPB_vect); // An empty ISR is needed. 
// Will not work without this.

void loop() {}

void setupLCD_I2C(void)
{
  // initialize LCD and set up the number of columns and rows: 
  lcd.begin(16, 2);
 
 
  writeLCD_I2C("PCF 8574","Address: 0x20   ");  // User function
  delay(1000); 
  writeLCD_I2C("PCF 8574","Ready for use");     // User function  
}
void writeLCD_I2C(String text1, String text2)
{
  lcd.clear();  // Clears the LCD screen
  lcd.setCursor(0,0);    lcd.print(text1); 
  lcd.setCursor(0,1);    lcd.print(text2);
  delay(1000); 
}
void writeLCD_I2C_TOP(String text1)
{
  lcd.clear();  // Clears the LCD screen
  lcd.setCursor(0,0);    lcd.print(text1); 
  delay(1000); 
}
void writeLCD_I2C_BOT(String text2)
{
  lcd.setCursor(0,1);    lcd.print(text2);
  delay(1000); 
}
void setupTimer(uint16_t FS)
{ 
  TCCR1A = 0;                 // set entire TCCR1A register to 0
  TCCR1B = 0;                 // same for TCCR1B
  TCCR1B |= (1 << WGM12);     // turn on CTC mode for Timer1
  if (FS >= 500) { 
      // Set CS12 CS11 CS10 bits for prescaler value 1
      preScalerT1Bits |= (0 << CS12) |(0 << CS11) | (1 << CS10);
      preScalerT1 = 1;
      preScalerCS1 = 1;       // CS1[2:0] 
  }
  if (FS >= 50 && FS < 500) { 
      // Set CS12 CS11 CS10 bits for prescaler value 8
      preScalerT1Bits |= (0 << CS12) |(1 << CS11) | (0 << CS10);
      preScalerT1 = 8;
      preScalerCS1 = 2;       // CS1[2:0]  
  }
  if (FS >= 5 && FS < 50) { 
      // Set CS12 CS11 CS10 bits for prescaler value 64
      preScalerT1Bits |= (0 << CS12) |(1 << CS11) | (1 << CS10);
      preScalerT1 = 64;
      preScalerCS1 = 3;       // CS1[2:0]  
  }
  if (FS >= 1 && FS < 5) { 
      // Set CS12 CS11 CS10 bits for prescaler value 256
      preScalerT1Bits |= (1 << CS12) |(0 << CS11) | (0 << CS10);
      preScalerT1 = 256;
      preScalerCS1 = 4;       // CS1[2:0]  
  }
  // set compare match register to match sampling frequency
  OCR1A = (16000000) / (FS * preScalerT1) - 1;
  OCR1B = OCR1A;
  TCNT1  = 0;                 //initialize counter value to 0
  TIMSK1 |= (1 << OCIE1B);    // enable timer compare interrupt to execute ADC ISR
 }
void startTimer(void)
{
   TCCR1B |= preScalerT1Bits;
}
void setupADC(uint16_t adcPin,uint16_t FS)
{
  analogReference(DEFAULT);     // Set ADC voltage reference to 5 V
  adcData = analogRead(adcPin); // Discard first conversion

  DIDR0 |= (1<<adcPin);   //Digital input disabled on all ADC ports
  ADCSRA |= ((1 << ADEN) | (1 << ADIE) | (1 << ADIF));  // turn ADC on, enable interrupt, lower flag 
  ADCSRA &= ~((1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0));    // clear ADC prescalar bits

  if (FS <= 8000) {
    ADCSRA |= ((1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0));
    adcPrescaler = 128;
  }
  if (FS > 8000 && FS <= 16000) {
    ADCSRA |= ((1 << ADPS2) | (1 << ADPS1) | (0 << ADPS0));
    adcPrescaler = 64;
  }
  if (FS > 16000 && FS <= 32000) {
    ADCSRA |= ((1 << ADPS2) | (0 << ADPS1) | (1 << ADPS0));
    adcPrescaler = 32;
  }
  if (FS > 32000 && FS < 64000) {
    ADCSRA |= ((1 << ADPS2) | (0 << ADPS1) | (0 << ADPS0));
    adcPrescaler = 16;
  }
   if (FS > 64000 && FS < 128000) {
    ADCSRA |= ((0 << ADPS2) | (1 << ADPS1) | (1 << ADPS0));
    adcPrescaler = 8;
  }
   if (FS > 128000 && FS < 256000) {
    ADCSRA |= ((0 << ADPS2) | (1 << ADPS1) | (0 << ADPS0));
    adcPrescaler = 4;
  }
   if (FS > 128000 && FS < 256000) {
    ADCSRA |= ((0 << ADPS2) | (0 << ADPS1) | (1 << ADPS0));
    adcPrescaler = 2;
  }  
  ADMUX &= ~((1<<REFS1) | (1<<REFS0) | (1<<ADLAR) | (1<<MUX3) | (1<<MUX2) | (1<<MUX1) | (1<<MUX0));
  ADMUX |= ((1<<REFS0) | (adcPin & 7));     // Sets voltage reference and ADC pin
  // select the A/D trigger source: timer1 compare B match (ADTS[2:0]=101)
  // A-D conversion is started when match occurs
  ADCSRB |= (0 << ACME) | (1 << ADTS2) | (0 << ADTS1) | (1 << ADTS0);
  ADCSRA |=  (1<<ADATE);                    // turn on automatic triggering
}
//
// Modified from:
// https://gist.github.com/wildan3105/e6b2b720342478cd6a1ee522c38d9c28
