// Fast Timer controlled ADC with buffering
// https://blog.wildan.us/2017/11/03/arduino-fast-er-sampling-rate/ 

const byte adcPin = 0;  // A0
const int MAX_RESULTS = 512;
volatile int results [MAX_RESULTS];
volatile int resultNumber;

// ADC complete ISR
ISR (ADC_vect)
  {
  if (resultNumber >= MAX_RESULTS)
    ADCSRA = 0;  // turn off ADC
  else
    results [resultNumber++] = ADC;
  }  // end of ADC_vect
  
EMPTY_INTERRUPT (TIMER1_COMPB_vect); 
// Essential. The timer issues an interrupt on Timer 1
// when counter equals OCR1B. Instead of running an
// ISR for Timer1 we run an ISR for the ADC.
// We can either define an ISR(TIMER1_COMPB_vect)
// with an empty function body, or we can tell that 
// this ISR is empty using the EMPTY_INTERRUPT()
// function.

void setup ()
  {
  Serial.begin(115200); // set baudrate
  Serial.println();
  // set Timer 1
  TCCR1A  = 0;
  TCCR1B  = 0;
  TCNT1   = 0;
  TCCR1B  = (1<<CS10) | (1<<WGM12);  // CTC, prescaler of 1
  TIMSK1  = (1<<OCIE1B);    // Execute ISR on interrupt 
  OCR1A   = 319;            // 20 us Timer cycle 
  OCR1B   = 319;            // 20 uS - sampling frequency 50 kHz
  // set ADC
  ADCSRA  =  (1<<ADEN) | (1<<ADIE) | (1<<ADIF); 
  // turn ADC on, want interrupt on completion, clear ADIF
  ADCSRA |= (1<<ADPS2);     // Prescaler of 16 (64 kHz)
  ADMUX   = (1<<REFS0) | (adcPin & 7);  // 5V ADC reference
  ADCSRB  = (1<<ADTS2) | (1<<ADTS0);    // Timer/Counter1 Compare Match B
  ADCSRA |= (1<<ADATE);     // turn on automatic triggering
}

void loop () {
  while (resultNumber < MAX_RESULTS) { } // wait until buffer is full   
  for (int i = 0; i < MAX_RESULTS; i++)
  {
    Serial.println (results [i]); 
    // Transmitting while ADC is stopped. No bottleneck here.
  }
  resultNumber = 0; // reset counter
  ADCSRA =  (1<<ADEN)|(1<<ADIE)|(1<<ADIF)|(1<<ADPS2)|(1<<ADATE); 
  // turn ADC ON and collect another sequence of samples
}
