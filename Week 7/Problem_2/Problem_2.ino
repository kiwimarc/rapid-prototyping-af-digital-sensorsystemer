int FS = 2000; // Not higher than 512 kHz
int numSamples = 1000; // Sample rate
int resultNumber = 0; // Number of samples
int preScalerT1 = 1;
int preScalerCS1 = 0;
byte preScalerT1Bits = 0; // Timer 1 prescalar bits CS[2:0]
uint8_t analogIn = 0; // Analog input pin
volatile uint16_t adcData; // ADC Data 
void setup() {
  cli(); // Stop interrups while setting timers

  // Timer Setup

  TCCR1A = 0; // Set TCCR1A register to 0
  TCCR1B = 0; // Set TCCR1B register to 0
  TCCR1B |= (1 << WGM12); // turn on CTC mode
  TCNT1 = 0; // Init counter value to 0

  if(FS >= 500) {
    // Set CS12 CS11 CS10 bits for prescaler value 1
    preScalerT1Bits |= (0 << CS12) | (0 << CS11) | (1<< CS10);
    preScalerT1 = 1;
    preScalerCS1 = 1; // CS1[2:0]
  }
  if(FS >=50 && FS < 500) {
    // Set CS12 CS11 CS10 bits for prescaler value 8
    preScalerT1Bits |= (0 << CS12) | (1 << CS11) | (0<< CS10);
    preScalerT1 = 8;
    preScalerCS1 = 2; // CS1[2:0]
  }
  if(FS >=5 && FS < 50) {
    // Set CS12 CS11 CS10 bits for prescaler value 64
    preScalerT1Bits |= (0 << CS12) | (1 << CS11) | (1<< CS10);
    preScalerT1 = 64;
    preScalerCS1 = 3; // CS1[2:0]
  }
  if(FS >=1 && FS < 5) {
    // Set CS12 CS11 CS10 bits for prescaler value 256
    preScalerT1Bits |= (1 << CS12) | (0 << CS11) | (0<< CS10);
    preScalerT1 = 256;
    preScalerCS1 = 4; // CS1[2:0]
  }

  // Set Timer1 compare match register to match samling frequency
  OCR1A = (16000000) / (FS * preScalerT1) - 1;
  OCR1B = 0; // Only used when ADC ISR is used
  TIMSK1 |= (1 << OCIE1A); // Enable timer compare conversion
  adcData = analogRead(analogIn); // Discard first conversion
  sei(); // Allow interrupts: SREG |= (1<<)
  delay(1000); // Wait for interupts to be enabled
  Serial.begin(115200);
  TCCR1B |= preScalerT1Bits; // Start Timer1
}

void loop() {}

ISR(TIMER1_COMPA_vect) {
  if (resultNumber++ < numSamples+1) {
    Serial.println(analogRead(analogIn)); // Send sample to serial
  }
  else {
    TIMSK1 &= ~(1 << OCIE1A); // Disable ISR execution
  }
}
