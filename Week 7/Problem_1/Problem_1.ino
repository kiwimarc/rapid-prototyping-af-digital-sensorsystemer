void setup() {
  Serial.begin(9600);
  Serial.println("-------------------------");
  Serial.print("ADMUX : ");
  ADMUX = B11100010; // ADMUX | CHANNEL 2 | 1.1V INTERNAL REF | LEFT ADJ
  Serial.print(ADMUX,16);
  Serial.print(" : ");
  Serial.println(ADMUX,2);

  Serial.print("ADCSRA init : ");
  Serial.print(ADCSRA,16);
  Serial.print(" : ");
  Serial.println(ADCSRA,2);
  
  Serial.print("ADCSRA zero : ");
  ADCSRA = B00000000; // ADCSRA | Zeroed
  Serial.print(ADCSRA,16);
  Serial.print(" : ");
  Serial.println(ADCSRA,2);

  Serial.print("ADCSRA final : ");
  ADCSRA = B00100110; // ADCSRA | Disabled ADC | Disable conversion | Prescaler 64 
  Serial.print(ADCSRA,16);
  Serial.print(" : ");
  Serial.println(ADCSRA,2);

  Serial.print("ADCSRB init : ");
  Serial.print(ADCSRB,16);
  Serial.print(" : ");
  Serial.println(ADCSRB,2);
  
  Serial.print("ADCSRB zero : ");
  ADCSRB = B00000000; // ADCSRB | Zeroed
  Serial.print(ADCSRB,16);
  Serial.print(" : ");
  Serial.println(ADCSRB,2);

  Serial.print("ADCSRB final : ");
  ADCSRB = B00000110; // ADCSRB | TRIGGER timer 1 overflow
  Serial.print(ADCSRB,16);
  Serial.print(" : ");
  Serial.println(ADCSRB,2);

}

void loop() {
}
