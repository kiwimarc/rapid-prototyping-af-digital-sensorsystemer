#include <Wire.h>
#define SLAVE_ADDRESS 0x08
int data = 0;
int state = 0;


void setup() {
  pinMode(13, OUTPUT);
  Serial.begin(9600);
  Wire.begin();

}

void loop() {
  Wire.beginTransmission(SLAVE_ADDRESS);
  Wire.write(1);
  Wire.endTransmission();
  digitalWrite(13,HIGH);
  delay(10);
  printLight();
  delay(1000);
  Wire.beginTransmission(SLAVE_ADDRESS);
  Wire.write(0);
  Wire.endTransmission();
  digitalWrite(13,LOW);
  delay(10);
  printLight(); //What is the slave's status?
  delay(200);
}

void printLight(){
  Wire.requestFrom(SLAVE_ADDRESS,1);
      //Request 1 byte from slave device
  data = Wire.read();
  switch (data){
    case 0:
      Serial.println("LED is OFF");
      break;
    case 1:
      Serial.println("LED is ON");
      break;
    default:
      Serial.println("Uknown status detected");
      break;
  }
}
