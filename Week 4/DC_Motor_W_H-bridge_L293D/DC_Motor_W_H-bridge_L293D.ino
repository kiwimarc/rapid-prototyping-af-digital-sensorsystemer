//H-bridge Motor Control
const int EN  = 9; //Half Bridge 1 Enable
const int MC1 = 3; //Motor Control 1
const int MC2 = 2; //Motor Control 2
const int POT = 0; //POT on Analog Pin 0

int val = 0; //Storing the reading
int vel = 0; //Storing the velocity (0-255)

void setup() {
  pinMode(EN, OUTPUT);
  pinMode(MC1, OUTPUT);
  pinMode(MC2, OUTPUT);
  brake();
  Serial.begin(115200);

}

void loop() {
  val = analogRead(POT);
  Serial.println(val);
  delay(50);

  //Go forward
  if (val > 562) {
    vel = map(val, 563, 1023, 0, 255);
    Serial.println("F");
    forward(vel);
  }
  // Go backward
  else if (val < 462) {
    vel = map(val, 461, 0, 0, 255);
    backward(vel);
  }
  //Brake
  else {
    brake();
  }
}
void forward(int rate) {
  digitalWrite(EN, LOW);
  digitalWrite(MC1, HIGH);
  digitalWrite(MC2, LOW);
  digitalWrite(EN, rate);
}
void backward(int rate) {
  digitalWrite(EN, LOW);
  digitalWrite(MC1, LOW);
  digitalWrite(MC2, HIGH);
  digitalWrite(EN, rate);
}
void brake() {
  digitalWrite(EN, LOW);
  digitalWrite(MC1, LOW);
  digitalWrite(MC2, LOW);
  digitalWrite(EN, HIGH);
}
