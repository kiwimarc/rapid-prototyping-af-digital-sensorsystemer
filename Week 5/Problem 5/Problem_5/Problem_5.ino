#include<LiquidCrystal.h>                        
LiquidCrystal lcd (7,8,9,10,11,12); // Define LCD display pins RS, E, D4, D5, D6, D7

int led = 13;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(led, OUTPUT);
  lcd.begin(16,2);                            
  lcd.setCursor(0,0);
  bitClear(PCICR,PCIE0);
  bitClear(PCMSK0,PCINT5);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("LED is on");
  delay(1000);                       // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("LED is off");
  delay(1000);                       // wait for a second
}
