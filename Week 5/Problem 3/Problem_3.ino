//
// Exploring Interrupt registers 

void setup(){
 Serial.begin(9600);
 Serial.println("-------------");
 Serial.println("Default status");
 Serial.print("SREG : ");
 Serial.print(SREG,16);
 Serial.print(" : ");
 Serial.println(SREG,2);

 Serial.print("EICRA : ");
 Serial.print(EICRA,16);
 Serial.print(" : ");
 Serial.println(EICRA,2);

 Serial.print("EIMSK : ");
 Serial.print(EIMSK,16);
 Serial.print(" : ");
 Serial.println(EIMSK,2);
 
 Serial.print("EIFR : ");
 Serial.print(EIFR,16);
 Serial.print(" : ");
 Serial.println(EIFR,2);

// Clearing bits
// 34: Disable global interrupt (bit 7 in SREG)
// 41: Set trigger mode to LOW on INT0 (ISC01, ISC00) = (0,0)
// 47: Set interrupt Diasble for INT0 and INT1 (INT1, INT0) = (0,0)
// 52: Set interrupt flags low for INT0 and INT1

 SREG &=~((1<<7));
 Serial.println("Status after bit clearing");
 Serial.print("SREG : ");
 Serial.print(SREG,16);
 Serial.print(" : ");
 Serial.println(SREG,2);

 EICRA&=~((1<<ISC01)|(1<<ISC00));
 Serial.print("EICRA : ");
 Serial.print(EICRA,16);
 Serial.print(" : ");
 Serial.println(EICRA,2);

 EIMSK&=~((1<<INT1)|(1<<INT0));
 Serial.print("EIMSK : ");
 Serial.print(EIMSK,16);
 Serial.print(" : ");
 Serial.println(EIMSK,2);

 EIFR&=~((1<<INTF1)|(1<<INTF0));
 Serial.print("EIFR : ");
 Serial.print(EIFR,16);
 Serial.print(" : ");
 Serial.println(EIFR,2);

// Setting bits
// 65: Enable Global interrupt (bit 7 in SREG)
// 72: Set trigger mode to Falling edge on INT0 (ISC01,ISC00) = (1,0)
// 78: Set interrupt Enable for INT0 (INT1, INT0) = (0,1)
// 85: SET interrupt flags low for INT0 and INT1 

 SREG |=(1<<7);
 Serial.println("Status after bit setting");
 Serial.print("SREG : ");
 Serial.print(SREG,16);
 Serial.print(" : ");
 Serial.println(SREG,2);

 EICRA |= ((1<<ISC01)|(0<<ISC00));
 Serial.print("EICRA : ");
 Serial.print(EICRA,16);
 Serial.print(" : ");
 Serial.println(EICRA,2);

 EIMSK |=((1<<INT0));
 Serial.print("EIMSK : ");
 Serial.print(EIMSK,16);
 Serial.print(" : ");
 Serial.println(EIMSK,2);

 // EIFR &=~((1<<INTF1)|(1<<INTF0)); //both versions sets bits low
 EIFR |= ((1<< INTF1) | (1<<INTF0));
 Serial.print("EIFR : ");
 Serial.print(EIFR,16);
 Serial.print(" : ");
 Serial.println(EIFR,2);
}
void loop() {
}
